//https://nodejs.org/dist/latest-v8.x/docs/api/fs.html


const fs = require('fs');


//////-----Synchronously Accessing File System-----/////
// read current directory files 
const files = fs.readdirSync('./'); 
//return string array
//console.log(files);


/////----Asynchronously Asccessing file System------//////
//RECOMMENDED

fs.readdir('./',function(err, files){

    if(err) console.log('Error' + err);
    else console.log('Result ' + files);
});


