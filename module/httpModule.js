

//https://nodejs.org/dist/latest-v8.x/docs/api/http.html


const http = require('http');
const server = http.createServer();

//here serevre object also extends EventEmitter functionalities

// ------>1 listening event
server.on('connection', (socket) => {

    console.log('New Connection...');
});


// -----> 2 so now triggering server event
server.listen(3000);
console.log('Listening on port 3000...')