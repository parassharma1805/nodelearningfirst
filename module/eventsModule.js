//https://nodejs.org/dist/latest-v8.x/docs/api/events.html#events_class_eventemitter


const EventEmitterClass = require('events');
const emitterObject = new EventEmitterClass();

//register a listener
emitterObject.on('MessageEvent', function(){

    console.log('Listener Activated');
});


//generating an event 
emitterObject.emit('MessageEvent'); 