

//we create custon class for event emitter


const EventEmitter = require('events');

class Logger extends EventEmitter
{
    log(message){

        //send an http request
        console.log(message);


        //raise an event

        this.emit('MsgLogged', {id : 1, url : 'http://'});

    }
}

module.exports = Logger;