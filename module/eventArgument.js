//we can pass arguements with each call back as follows :


const EventEmitterClass = require('events');
const emitterObj = new EventEmitterClass();

//regitser a listener
//use : (arg) =>   inplace of  function(arg)
emitterObj.on('MsgLogged', (arg) => {

    console.log('Listener Invoked', arg);
});


//raise an event
emitterObj.emit('MsgLogged', {id : 1, url:'http://'});