
//So at the core of node we have a module
//Every file in node is considered as a module.
//Every fn or var defined in this file are
//private to this file. That is why we got
//REFERENCE ERROR becouse we are running in
//node environment , so thats why window object 
//doesnt work here.

//so will consider this js file as one module
//lets find what we have in this file module.
console.log(module);
//Output : JSON Info

//NEXT we will see how to create and load
//a module.
       
