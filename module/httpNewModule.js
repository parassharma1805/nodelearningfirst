

const http = require('http');

//get the callback here in this
const server = http.createServer((req,res) => {

    if(req.url === '/')
    {
        //API 1
        //got request from client
        //---send something to client
        res.write('Hello World Test');
        res.end();
        //server.close();
    
    }

    if(req.url === '/api/courses')
    {
        //API 2
        //return list of courses using JSON format
        res.write(JSON.stringify([1,2,3]));
        res.end();
    }

});

server.listen(5000);
console.log('Listening on port 5000...')