
// logger message example :

//(function (exports, require, module, __filename, __dirname) { ...all the below code inside  })

console.log(__filename);
console.log(__dirname);

var url = 'http://mylogger.io/log'

function log(message){
    //send an http request
    console.log(message);
}

//above var 'url' and function 'log' are both scope to this module.
//they are private.
//both of these are not visible to outside world or other module.
//However, in app.js (main module) we wana use this logger.
//So app.js should be able to access both function. So we need to make 
//both of this public.
//So How ? 
//To do this we can use export object that we have seen in 
//previous run (modules.js), just run and you can get this object.
//    export : {}   ---->>>     so anything we added to this will get 
//                              exported and will be available outside 
//                              of this module.


//So this is how it goes : exporting log function
module.exports.log = log;

//similarly for var url
//module.exports.urlPoint = url;

//Now the next step we want to load a module. To do this we can use :
//     require(target address of fn)  -->>  use this function in our app module 
