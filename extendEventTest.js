
//EXAMPLE TEST : extendEventEmitter



//catch here the exported module 
// -----> 1 Innitialization logger class
const Logger = require('./module/extendEventEmitter');
const logger = new Logger();


//   -----> 3 get the callback 
logger.on('MsgLogged', (arg) => {

    console.log('Listener Called', arg);

});

//  ---> 2 called log method and callback nside log method
logger.log('Event Triggered')