 
//global method it can be accessed from any part of the project.
console.log();

//Used for timeout. It can be run from browser and node.
setTimeout(); 
clearTimeout();

//used to call a function repeatedly after a given delay
setInterval();

//stop the function to call repeatedly
clearInterval();

//So above are the global objects in javaScript
//in node we have some other objects

//in browser we have window object
//so internaly by default browser use windpw object to initialize 
//all global variables
//ex : 
window.console.log();
// all the objects belong to the window objoects

//but in node we dnt have this window object
//we have global object
//example :

global.clearInterval();
global.setTimeout();

//etc.
