

//var message is not a part of global
var message = '';
console.log(global.message);

//In the client side javaScript run inside an browser when we 
//declare a var or function that is added to the global scope.
//ex : the fn added to the global scope
var sayHello = function(){

    console.log('Msg');
}
// and it is available by the window object
window.sayHello(); //REFERENCE ERROR : The window object 
//represents an open window in a browser. Since you are not running your 
//code within a browser but via windows script host, 
//the interpreter won't be able to find the window object 
//since it does not exist since you're not within a web browser.
//WILL RUN IN BROWSER ----->
//but there is a problem
//in real world project you may have number of files and it is possible
//the same function is defined in more than one file. Becouse 
//of this the functionality will we overriden for this fn for each new 
//file call which contains this method.


//so we go with modules