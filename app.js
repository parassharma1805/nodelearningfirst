//var x = ;

//function sayHello(name) {
//   console.log('Hello ' + name);
//}
//sayHello('Paras');
//console.log(window);


///////-------------THIS IS OUR MAIN MODULE : app.js-----------/////////////////
///////-----Loading Modules------///////
//To load the mopdule we have require function :

// require('./exLoggerModule');  -->> if exLoggerModule present in same directory  
// require('./subfolder/exLoggerModule');  -->> if exLoggerModule in subfolder
// require('../exLoggerModule');  -->> if exLoggerModule in parent folder

//So in our case the loggerModule is in subfolder. Hence :

var loggerModule = require('./module/exLoggerModule');
//lets log this
console.log(loggerModule);
//We will get output : { log: [Function: log] }
//so now log contains a function in which we can pass msg
loggerModule.log('Export Module Test : Success Message');

//How Node env does this : Means how in node environment modules concept are working.
//Ans :  Node env directly not executes our code. It envelopes the complete code module 
//inside a function. Like as follows : 
//   (function (exports, require, module, __filename, __dirname) {

                    //var url = 'http://mylogger.io/log'

                    //function log(message){
                    //send an http request
                    //console.log(message);
                    //}
//      })
//But we dnt have to take care of this above function. It is internally handled by node.

//Also node uses some of internally build mmodules. That we are going to use in next topic.



